#!/bin/sh

# Extraction script for:
# Metroid Prime (Europe) (En,Fr,De,Es,It).iso

# Outputs:
# - Metroid (USA).nes

# Requires: wit, decryptmetroid

# Thanks to FIX94 releasing decryptmetroid.c under https://gist.github.com/FIX94/7593640c5cee6c37e3b23e7fcf8fe5b7

romextract()
{
	dependency_wit            || return 1
	dependency_decryptmetroid || return 1

	# TODO: Maybe the Europe ROM can be found in NESPALemu.rel
	echo "Extracting file from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
	--files=+/files/NESPAL60emu.rel

	echo "Extracting ROM ..."
	"$DECRYPTMETROID_PATH" P "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GM8P/files/NESPAL60emu.rel" \
		"$SCRIPTID/Metroid (USA).nes"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
