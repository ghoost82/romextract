#!/bin/sh

# Extraction script for:
# Déjà Vu: MacVenture Series (dejavu.exe)

# Outputs:
# - Deja Vu.2mg
# - Deja Vu.dsk

romextract()
{

	echo "Extracting game files from dejavu.exe ..."
	# tail for offset, head for game file size
	tail -c +594329 "$FILE" | head -c +819271 \
		> "$SCRIPTID/Deja Vu.2mg"
	tail -c +1413601 "$FILE" | head -c +819200 \
		> "$SCRIPTID/Deja Vu.dsk"

	echo "Script $SCRIPTID.sh done"
}
